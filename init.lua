--[[

    This Mod is a separation and development of Sokomines handmill and
    threshing floor provided by her cottages mod.

    Copyright (C) 2023  Nordal
    Copyright (C) 2013  Sokomine 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

--]]

--get translator
local S = minetest.get_translator(minetest.get_current_modname())

local version = "1.1"
local mt_version = "5.5+"

--read settings
local settings = minetest.settings
local seed_percent = tonumber(settings:get("threshing_floor_yield_seeds")) or 50
local straw_percent = tonumber(settings:get("threshing_floor_yield_straw")) or 20

-- ### helper functions ###

local valid_grain = {
	["farming:wheat"] = "farming:seed_wheat",
	["farming:rye"] = "farming:seed_rye",
	["farming:barley"] = "farming:seed_barley",
	["farming:oat"] = "farming:seed_oat"
}

local is_valid_grain = function(item)
	for k, v in pairs(valid_grain) do
		if k == item then
			return true
		end
	end
	return false
end

local get_seed = function(item)
	for k, v in pairs(valid_grain) do
		if k == item then
			return v
		end
	end
	return false
end

local is_valid_seeds = function(item)
	for k, v in pairs(valid_grain) do
		if item == v then
			return true
		end
	end
	if item == "farming:rice" then
		return true
	end
	return false
end

-- ### threshing floor ###

local threshing_floor_formspec = function()
	return "formspec_version[5]" ..
		"size[10.75,7.88]" ..
		"list[context;src;3,0.75;1,1;]" ..
		"tooltip[src;" .. S("Put harvested grain here: wheat, oat, barley or rye") .. "]" ..
		"image[4.25,0.75;1,1;gui_furnace_arrow_bg.png^[transformR270]" ..
		"list[context;dst_straw;5.5,0.75;1,1;]" ..
		"tooltip[dst_straw;" .. S("Get straw and seeds punching the floor with stick") .. "]" ..
		"list[context;dst_seeds;6.75,0.75;1,1;]" ..
		"tooltip[dst_seeds;" .. S("Get straw and seeds punching the floor with stick") .. "]" ..
		"list[current_player;main;0.5,2.5;8,1;]" ..
		"list[current_player;main;0.5,3.88;8,3;8]" ..
		"listring[context;dst_straw]" ..
		"listring[current_player;main]" ..
		"listring[context;dst_seeds]" ..
		"listring[current_player;main]" ..
		"listring[context;src]" ..
		"listring[current_player;main]"
end

minetest.register_node("handmill:threshing_floor", {
	drawtype = "nodebox",
	description = S("Threshing Floor. Punch with stick."),
	tiles = {"handmill_cottages_junglewood.png^farming_wheat.png", 
	    "handmill_cottages_minimal_wood.png",
		"handmill_cottages_minimal_wood.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	groups = {choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	is_ground_content = false,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.50, -0.5, -0.50, 0.50, -0.40, 0.50},

			{-0.50, -0.4, -0.50, -0.45, -0.20, 0.50},
			{0.45, -0.4, -0.50, 0.50, -0.20, 0.50},

			{-0.45, -0.4, -0.50, 0.45, -0.20, -0.45},
			{-0.45, -0.4, 0.45, 0.45, -0.20, 0.50},
		}
	},
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.50, -0.5, -0.50, 0.50, -0.20, 0.50},
		}
	},

	on_construct = function(pos)

		local meta = minetest.get_meta(pos)

		meta:set_string("formspec", threshing_floor_formspec())
		meta:set_string("infotext", S("Threshing Floor"))

		local inv = meta:get_inventory()

		inv:set_size("src", 1) --grain
		inv:set_size("dst_straw", 1) --straw
		inv:set_size("dst_seeds", 1) --seeds
	end,

	can_dig = function(pos, player)

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()

		if not inv:is_empty("src") or not inv:is_empty("dst_straw") or not inv:is_empty("dst_seeds") then
			return false
		end

		return true
	end,

	allow_metadata_inventory_take = function(pos, listname, index, stack, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		return stack:get_count()
	end,

	allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		if to_list == "src" then
			return count

		elseif to_list == "dst_straw" then
			return 0

		elseif to_list == "dst_seeds" then
			return 0
		end
	end,

	allow_metadata_inventory_put = function(pos, listname, index, stack, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		--don't allow put content to src other than valid grain
		if listname == "src" then

			if is_valid_grain(stack:get_name()) then
				return stack:get_count()
			else
				return 0
			end

			--don't allow put anything to dst lists
		elseif listname == "dst_straw" or listname == "dst_seeds" then

			return 0
		end
	end,

	on_punch = function(pos, node, puncher)

		local wielded = puncher:get_wielded_item()

		if not wielded or wielded == "" then
			return
		end

		--thresh grain with stick
		if wielded:get_name() ~= "default:stick" then
			return
		end

		local name = puncher:get_player_name()
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local src_stack = inv:get_stack("src", 1)

		if src_stack:is_empty() then
			minetest.chat_send_player(name, S('Threshing floor is empty.'))
			return
		end

		local grain = src_stack:get_name()
		local grain_count = src_stack:get_count()
		local threshed = math.random(6, 18)

		--threshed shall not be more than left grain
		if grain_count < threshed then
			threshed = grain_count
		end

		local straw = math.floor(threshed * straw_percent / 100)
		local seeds = math.ceil(threshed * seed_percent / 100)

		if inv:room_for_item("dst_straw", "farming:straw " .. tostring(straw)) and
			inv:room_for_item("dst_seeds", get_seed(grain) .. " " .. tostring(seeds)) then
			--add straw and seeds, remove threshed grain
			inv:add_item("dst_straw", "farming:straw " .. tostring(straw))
			inv:add_item("dst_seeds", get_seed(grain) .. " " .. tostring(seeds))
			inv:remove_item("src", grain .. " " .. tostring(threshed))

			local grain_left = grain_count - threshed

			if grain_left > 0 then
				minetest.chat_send_player(name, S('%s threshed, %s left.'):format(threshed, grain_left))
			else
				minetest.chat_send_player(name, S('No grain left.'))
			end
		end
	end
})

-- ### handmill ###

local handmill_formspec = function()
	return "formspec_version[5]" ..
		"size[10.75,7.88]" ..
		"list[context;src;3.63,0.75;1,1;]" ..
		"image[4.88,0.75;1,1;gui_furnace_arrow_bg.png^[transformR270]" ..
		"list[context;dst;6.13,0.75;1,1;]" ..
		"list[current_player;main;0.5,2.5;8,1;]" ..
		"list[current_player;main;0.5,3.88;8,3;8]" ..
		"listring[context;dst]" ..
		"listring[current_player;main]" ..
		"listring[context;src]" ..
		"listring[current_player;main]"
end

minetest.register_node("handmill:handmill", {
	description = S("Handmill, powered by punching"),
	drawtype = "mesh",
	mesh = "handmill_cottages_handmill.obj",
	tiles = {"handmill_cottages_stone.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	groups = {cracky = 2},
	is_ground_content = false,
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.50, -0.5, -0.50, 0.50, 0.25, 0.50},
		}
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.50, -0.5, -0.50, 0.50, 0.25, 0.50},
		}
	},

	on_construct = function(pos)

		local meta = minetest.get_meta(pos)

		meta:set_string("formspec", handmill_formspec())
		meta:set_string("infotext", S("Handmill"))

		local inv = meta:get_inventory()

		inv:set_size("src", 1)
		inv:set_size("dst", 1)
	end,

	can_dig = function(pos, player)

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()

		if not inv:is_empty("src")
			or not inv:is_empty("dst") then
			return false
		end

		return true
	end,

	allow_metadata_inventory_take = function(pos, listname, index, stack, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		return stack:get_count()
	end,

	allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		if to_list == "src" then
			return count
		elseif to_list == "dst" then
			return 0
		end
	end,

	allow_metadata_inventory_put = function(pos, listname, index, stack, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		--don't allow put content to src other than valid seeds
		if listname == "src" then

			if is_valid_seeds(stack:get_name()) then
				return stack:get_count()
			else
				return 0
			end

			--don't allow put anything to dst list
		elseif listname == "dst" then

			return 0
		end
	end,

	on_punch = function(pos, node, puncher)

		local name = puncher:get_player_name()
		local wielded = puncher:get_wielded_item()

		if not wielded:is_empty() then
			--turn millstone by hand
			minetest.chat_send_player(name, S('Punch handmill with hand.'))
			return
		end

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()

		local src_stack = inv:get_stack("src", 1)

		if src_stack:is_empty() then
			minetest.chat_send_player(name, S('Handmill is empty.'))
			return
		end

		local seeds_name = src_stack:get_name()
		local flour = "farming:flour"

		if seeds_name == "farming:rice" then
			flour = "farming:rice_flour"
		end

		local seeds_count = src_stack:get_count()
		local ground = math.random(5, 15)

		--ground shall not be more than left seeds
		if seeds_count < ground then
			ground = seeds_count
		end

		if inv:room_for_item("dst", flour .. " " .. tostring(ground)) then

			inv:add_item("dst", flour .. " " .. tostring(ground))
			inv:remove_item("src", seeds_name .. " " .. tostring(ground))

			local seeds_left = seeds_count - ground

			if seeds_left > 0 then
				minetest.chat_send_player(name, S('%s ground, %s left.'):format(ground, seeds_left))
			else
				minetest.chat_send_player(name, S('No seeds left.'))
			end

			--turn mill
			node.param2 = node.param2 + 1
			if node.param2 > 3 then
				node.param2 = 0
			end
			minetest.swap_node(pos, node)
		end
	end
})

-- ### multigrain handmill ###

local multigrain_handmill_formspec = function()
	return "formspec_version[5]" ..
		"size[10.75,9.13]" ..
		"list[context;src;2.38,0.75;2,2;]" ..
		"image[4.88,1.38;1,1;gui_furnace_arrow_bg.png^[transformR270]" ..
		"list[context;dst;6,0.75;2,2;]" ..
		"list[current_player;main;0.5,3.75;8,1;]" ..
		"list[current_player;main;0.5,5.13;8,3;8]" ..
		"listring[context;dst]" ..
		"listring[current_player;main]" ..
		"listring[context;src]" ..
		"listring[current_player;main]"
end

minetest.register_node("handmill:multigrain_handmill", {
	description = S("Multigrain Handmill, powered by punching"),
	drawtype = "mesh",
	mesh = "handmill_cottages_handmill.obj",
	tiles = {"default_desert_stone.png"},
	paramtype = "light",
	paramtype2 = "facedir",
	groups = {cracky = 2},
	is_ground_content = false,
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.50, -0.5, -0.50, 0.50, 0.25, 0.50},
		}
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.50, -0.5, -0.50, 0.50, 0.25, 0.50},
		}
	},

	on_construct = function(pos)

		local meta = minetest.get_meta(pos)

		meta:set_string("formspec", multigrain_handmill_formspec())
		meta:set_string("infotext", S("Multigrain Handmill"))

		local inv = meta:get_inventory()

		inv:set_size("src", 4)
		inv:set_size("dst", 4)
	end,

	can_dig = function(pos, player)

		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()

		if not inv:is_empty("src")
			or not inv:is_empty("dst") then
			return false
		end

		return true
	end,

	allow_metadata_inventory_take = function(pos, listname, index, stack, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		return stack:get_count()
	end,

	allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		if to_list == "src" then
			return count
		elseif to_list == "dst" then
			return 0
		end
	end,

	allow_metadata_inventory_put = function(pos, listname, index, stack, player)

		if minetest.is_protected(pos, player:get_player_name()) then
			return 0
		end

		--don't allow put content to src other than valid seeds
		if listname == "src" then

			if is_valid_seeds(stack:get_name()) then
				if stack:get_name() == "farming:rice" then
					return 0
				else
					return stack:get_count()
				end
			else
				return 0
			end

			--don't allow put anything to dst list
		elseif listname == "dst" then

			return 0
		end
	end,

	on_punch = function(pos, node, puncher)

		local wielded = puncher:get_wielded_item()

		--turn millstone by hand, "" -> empty stack -> no wielded item
		if wielded ~= ItemStack("") then
			return
		end

		local name = puncher:get_player_name()
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local fullstacks = {}
		local seeds_min = 99

		for i = 1, 4 do
			local srcstack = inv:get_stack("src", i)
			if not srcstack:is_empty() then
				table.insert(fullstacks, {["name"] = srcstack:get_name(), ["count"] = srcstack:get_count()})
				--get smallest stack in case number of seeds in inv slots is not equal
				local c = srcstack:get_count()
				if seeds_min > c then
					seeds_min = c
				end
			end
		end

		if not fullstacks or #fullstacks < 1 then
			minetest.chat_send_player(name, S('Multigrain Handmill is empty.'))
			return
		end

		if #fullstacks < 4 then
			minetest.chat_send_player(name, S('Input is incomplete.'))
			return
		end

		if fullstacks[1].name ~= fullstacks[2].name and fullstacks[1].name ~= fullstacks[3].name
			and fullstacks[1].name ~= fullstacks[4].name and fullstacks[2].name ~= fullstacks[3].name
			and fullstacks[2].name ~= fullstacks[4].name and fullstacks[3].name ~= fullstacks[4].name then

		else
			minetest.chat_send_player(name, S('Input four different grain seeds.'))
			return
		end

		local ground = math.random(5, 15)

		--ground shall not be more than smallest number of seeds in inv stacks
		if seeds_min < ground then
			ground = seeds_min
		end

		local flour = ground * 4

		if inv:room_for_item("dst", "farming:flour_multigrain " .. tostring(flour)) then
			inv:add_item("dst", "farming:flour_multigrain " .. tostring(flour))

			local no_seeds = {}
			for i = 1, 4 do
				inv:remove_item("src", fullstacks[i].name .. " " .. tostring(ground)) --"src", "farming:seed_oat 12"
				local stack = inv:get_stack("src", i)
				if stack:is_empty() then
					table.insert(no_seeds, fullstacks[i].name)
				end
			end

			local seeds_left = seeds_min - ground

			if seeds_left > 0 then
				minetest.chat_send_player(name, S('%s ground, %s left.'):format(ground, seeds_left))
			else
				minetest.chat_send_player(name, S('No seeds left.'))
			end

			--turn mill
			node.param2 = node.param2 + 1
			if node.param2 > 3 then
				node.param2 = 0
			end
			minetest.swap_node(pos, node)
		end
	end
})

-- ### crafting ###

minetest.register_craft({
	output = "handmill:threshing_floor",
	recipe = {
		{"group:wood", "farming:wheat", "group:wood"},
		{"group:wood", "group:wood", "group:wood"},
	}
})

minetest.register_craft({
	output = "handmill:handmill",
	recipe = {
		{"default:stick", "default:stone", ""},
		{"", "group:wood", ""},
		{"", "default:stone", ""}
	}
})

minetest.register_craft({
	output = "handmill:multigrain_handmill",
	recipe = {
		{"default:stick", "default:desert_stone", ""},
		{"", "group:wood", ""},
		{"", "default:desert_stone", ""}
	}
})
print("[Mod] Handmill loaded")
