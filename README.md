# Handmill Mod

Handmill is a logical and realistic way to get seeds and straw from grain and flour from seeds. Obtain more seeds, more straw and more flour than by crafting recipes of farming mods. You can configure the amount of seeds and straw you get via settings.

This Mod is a separation and development of Sokomines handmill and threshing floor provided by her cottages mod. The formspecs are modernized and simplified, functionality is extended. The mod also adds a new multigrain handmill. 

The use with Farming Redo mod is strongly recommended, because it introduces the four main grains and their seeds along with flour. But handmill mod is also working without it. So you can use handmill with the farming mod of minetest game. 

## Threshing floor

The threshing floor can process the four grains of farming redo. The output of seeds and straw is configurable via settings.

## Handmill

The handmill grinds flour plus rice flour according to the input, which can be rice and seeds of wheat, barley, oat, rye.

## Multigrain Handmill

The mod adds a new multigrain handmill that produces multigrain flour from the four grain seeds.

## Code Licence 

Licence of the code is GPLv3. The Code is completely rewritten. Only some parts are written following the cottages mod's code.

## Graphics Licence

1. CC-BY-SA 3.0:

* handmill_model
* cottages_minimal_wood.png by celeron55

2. WTFPL: 

* cottages_stone.png by Cisoun,
* cottages_junglewood.png by PilzAdam





