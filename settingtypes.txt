# Amount of seeds you obtain from using the threshing floor in percent of grain to be threshed.
# Must be a number in the range of 1 - 100. Default 50.
threshing_floor_yield_seeds (Threshing Floor Yield Seeds) int 50

# Amount of straw you obtain from using the threshing floor in percent of grain to be threshed.
# Must be a number in the range of 1 - 100. Default 20.
threshing_floor_yield_straw (Threshing Floor Yield Straw) int 20
